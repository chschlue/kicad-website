+++
title = "ToucheLibre"
projectdeveloper = "Lilian Tribouilloy"
projecturl = "http://touchelibre.fr"
"made-with-kicad/categories" = [
    "USB Device",
    "Computer",
    "Human / Machine Interface"
]
+++

ToucheLibre is an ergonomic wooden keyboard. Designed under
https://gitlab.com/touchelibre/Clavier_TouLi-01/-/tree/master/03_%C3%89lectronique/01_Sch%C3%A9ma_Routage/[openhardware]
philosophie and with ecological sensitivity.

Be the luthier of your writings.

This is a summary of our values: to make a beautifull artisanal objet;
an useful object in according to your needs and your health; to give
an artistic look to inspire your work; a handshaking between craftsman
and customer.

