/*!
 * KiCad js
 */

(function() {

    if ($('.navbar').hasClass('transparent')) {
        $(window).scroll(function() {
            if ($(this).scrollTop() < 430) {
                $('.navbar').addClass('transparent');
            } else {
                $('.navbar').removeClass('transparent');
            }
        });
    }

    $(window).resize(function() {
        var mobileLimit = 760;
        if ($(window).width() < mobileLimit) {
            $('.navbar').addClass('navbar-static-top');
            $('.navbar').removeClass('navbar-fixed-top');
            $('header').addClass('page-header-decor-no-margin');
        } else {
            $('.navbar').removeClass('navbar-static-top');
            $('.navbar').addClass('navbar-fixed-top');
            $('header').removeClass('page-header-decor-no-margin');
        }
    });
    $(window).trigger('resize');

})();

/*
 * Add Donate thank you page during download
 */
$(".dl-link").click(function() {
    $(".initial-text").hide();
    $(".page-heading").hide();
    $(".donate-hidden").show();
    $(".backup-link")[0].href = this.href;
})

$(".osdn").click(function() {
    var i = document.createElement('iframe');
    i.style.display = 'none';
    var uri = parseUri(this.href);
    i.src = 'https://osdn.net/frs/redir.php?m=constant&f=/storage/g/k/ki/kicad/' + uri.file;
    document.body.appendChild(i);
    return false;
})

// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License

function parseUri(str) {
    var o = parseUri.options,
        m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
};

parseUri.options = {
    strictMode: false,
    key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
    q: {
        name: "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};